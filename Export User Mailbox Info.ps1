#**************** Step W1. Get Creds. ****************#
function GetCreds {
    write-host "Step Y. Get Creds." -ForegroundColor Yellow

    write-host "Importing Office 365 PowerShell Commandlets" 
    Write-Host -ForegroundColor White -BackgroundColor DarkBlue "*** PLEASE ENTER OFFICE 365 ADMIN CREDENTIALS ***" 
        
    #The below stores the admin username in a plain text file.
    Read-Host "Enter Username" | Out-File "C:\Temp\Username.txt"
    #The below stores the admin password in an encrypted text file, only decryptable to the UID it is run on.
    Read-Host "Enter Password" -AsSecureString |  ConvertFrom-SecureString | Out-File "C:\Temp\Password.txt"
}

#**************** Step Y1. Connects to Office 365. ****************#
function connect_o365 {
    #Pulls the stored creds and uses them to login to Office 365.
    $AdminUsername = Get-Content C:\Temp\Username.txt
    $EncryptedPass = Get-Content C:\Temp\Password.txt | ConvertTo-SecureString
    $credential = New-Object System.Management.Automation.PsCredential($AdminUsername, $EncryptedPass)

    #Connects to Office 365.
    Connect-MsolService -Credential $credential
    $Session = New-PSSession -ConfigurationName Microsoft.Exchange -ConnectionUri "https://ps.outlook.com/powershell/" -Credential $credential -Authentication Basic -AllowRedirection
    Import-PSSession $Session
    Connect-AzureAD -Credential $credential
}

#**************** Step W2. Get Creds for MFA connection. ****************#
function GetCredsMFA {
    write-host "Step Y. Get Creds." -ForegroundColor Yellow

    write-host "Importing Office 365 PowerShell Commandlets" 
    Write-Host -ForegroundColor White -BackgroundColor DarkBlue "*** PLEASE ENTER OFFICE 365 ADMIN CREDENTIALS ***" 
        
    #The below stores the admin username in a plain text file.
    Read-Host "Enter Username" | Out-File "C:\Temp\Username.txt"
    
    <#The below doesn't work with 2FA/MFA sesssions.
    The below stores the admin password in an encrypted text file, only decryptable to the UID it is run on.
    Read-Host "Enter Password" -AsSecureString |  ConvertFrom-SecureString | Out-File "C:\Temp\Password.txt"#>
}

#**************** Step Y2. Connects to Office 365 for MFA connection. ****************#
function connect_o365MFA {
    
    $AdminUsername = Get-Content C:\Temp\Username.txt
    
    $PSExoPowershellModuleRoot = (Get-ChildItem -Path $env:userprofile -Filter CreateExoPSSession.ps1 -Recurse -ErrorAction SilentlyContinue -Force | Select -Last 1).DirectoryName
    $ExoPowershellModule = "Microsoft.Exchange.Management.ExoPowershellModule.dll";
    $ModulePath = [System.IO.Path]::Combine($PSExoPowershellModuleRoot, $ExoPowershellModule);

    Import-Module $ModulePath;

    $Office365PSSession = New-ExoPSSession -UserPrincipalName $AdminUsername -ConnectionUri "https://outlook.office365.com/powershell-liveid/"
    write-host "This will take a couple of minutes." -ForegroundColor Yellow
    
    Import-PSSession $Office365PSSession
    Connect-AzureAD -AccountId $AdminUsername #-AccountId seems to pass through the session token to the Azure login.
    
}

#**************** Step Z. Seclect connection type. ****************#
function ConnectionType {
    write-host "Step Z. Select a connection type."
    write-host "1. Non MFA"
    write-host "2. MFA"
    $ConnectionOption = Read-Host "Please select the connection type:"

    if ($ConnectionOption -eq 1) {
        GetCreds
        connect_o365

    }elseif ($ConnectionOption -eq 2) {
        GetCredsMFA
        connect_o365MFA

    }else{
        write-host "**********************************************" -ForegroundColor Red
        write-host "Please select one of the above options" -ForegroundColor Gray
        write-host "**********************************************" -ForegroundColor Red
        write-host ""
        Start-Sleep -Seconds 2
        ConnectionType
    }
}
ConnectionType

#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%#

Get-MSOLUser | Where-Object { $_.isLicensed -eq "True"} | Select-Object FirstName, LastName, UserPrincipalName | Export-Csv C:\Temp\UserExport.csv

#Get-Recipient | Select DisplayName, RecipientType, EmailAddresses | Export-Csv C:\Temp\EmailAddresses.csv
