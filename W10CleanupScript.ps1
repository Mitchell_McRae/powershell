$ErrorActionPreference = "Continue"
<# 
.NOTES 
=========================================================================== 
Run script as admin: https://www.experts-exchange.com/questions/29111358/PowerShell-Script-Force-script-to-always-Run-As-Administrator.html

1&2.    https://www.maketecheasier.com/uninstall-pre-installed-apps-windows-10/
19      https://www.itninja.com/blog/view/import-startlayout-kace-sda
        https://www.tutorialspoint.com/powershell/powershell_filesio_create_xml.htm
23.     https://www.reddit.com/r/PowerShell/comments/8h1s2u/powershell_script_to_uninstall_software/
=========================================================================== 
Version 1.1 Mitchell McRae
20211222
=========================================================================== 
Powershell modules required.
None
===========================================================================
.DESCRIPTION 
01. Uninstalls Windows Store apps.
02. Disables the ability from them to come back. This has been remvoed due to issues with removing the Windows Store App.
03. Disables suggested apps in the start menu.
04. Removes the People button from the taskbar.
05. Hides the Task View button from the taskbar
06. Hides the Cortana Search Icon/Box from the taskbar.
07. Disables location tracking.
09. Disables automatic map updates.
10. Disables feedback.
11. Disable tailored experiences.
12. Disable the machines advertising ID.
13. Disables error reporting.
14. Enables numLock after startup.
15. Show known file extensions.
16. Changes the default Windows Explorer view to this PC.
17. Changes the default Start Menu layout to a blank template.
18. Add C:\Temp Folder - might not be required.
19. Installs Chrome.
20. Installs Adobe Reader DC.
21. Askes you what you want to rename the machine to.
22. Restarts the computer.
23. Removes Pre Installed Office.
24. Install 7Zip
25. Install VLC
26. Install OpenVPN
27. Install MS Teams
#>

# Run script as admin
# This can not be run in a function because it has to be running for the whole script.
<#If (-NOT ([Security.Principal.WindowsPrincipal] [Security.Principal.WindowsIdentity]::GetCurrent()).IsInRole([Security.Principal.WindowsBuiltInRole]::Administrator))
{
  # Relaunch as an elevated process:
  Start-Process powershell.exe "-File",('"{0}"' -f $MyInvocation.MyCommand.Path) -Verb RunAs
  exit
}#>

function YesOrNo {
    write-host "**********************************************" -ForegroundColor Red
    write-host "Please enter y/n" -ForegroundColor Gray
    write-host "**********************************************" -ForegroundColor Red
    write-host ""
}

# Uninstalling Windows apps
function RemoveCrap {
    write-host " Step 1: Uninstalling Windows apps"
    #I have added a 5 second pause after each section, because it seemed to of resolve an issue where the Windows Store would be removed.
    
    #Games
    Get-AppxPackage -allusers *KING* | Remove-AppxPackage
    Get-appxpackage -allusers *solitaire* | remove-appxpackage
    Get-appxpackage -allusers *xbox* | remove-appxpackage #Works but errors on one app
    Get-AppxPackage -allusers *HiddenCityMysteryofShadows* | Remove-AppxPackage
    Get-AppxPackage -allusers *AutodeskSketchBook* | Remove-AppxPackage
    Get-AppxPackage -allusers *Disney* | Remove-AppxPackage
    Get-AppxPackage -allusers *MarchofEmpires* | Remove-AppxPackage
    Get-AppxPackage -allusers *FarmVille* | Remove-AppxPackage
    Get-AppxPackage -allusers *Duolingo* | Remove-AppxPackage
    Get-AppxPackage -allusers *RoyalRevolt2* | Remove-AppxPackage
    Get-AppxPackage -allusers *Asphalt8Airborne* | Remove-AppxPackage
    Get-AppxPackage -allusers *Keeper* | Remove-AppxPackage
    Get-AppxPackage -allusers *BubbleWitch3Saga* | Remove-AppxPackage
    Get-AppxPackage -allusers *CandyCrushSodaSaga* | Remove-AppxPackage
    Get-AppxPackage -allusers *PandoraMediaInc* | Remove-AppxPackage
    Get-AppxPackage -allusers *XING* | Remove-AppxPackage
    Start-Sleep -Seconds 5
    
    #Software
    Get-appxpackage -allusers *3d* | remove-appxpackage
    Get-AppxPackage -allusers *ActiproSoftwareLLC* | Remove-AppxPackage
    Get-AppxPackage -allusers *Adobe* | Remove-AppxPackage
    Get-AppxPackage -allusers *Axilesoft* | Remove-AppxPackage
    Get-AppxPackage -allusers *CyberLink* | Remove-AppxPackage
    Get-AppxPackage -allusers *Disney* | Remove-AppxPackage
    Get-AppxPackage -allusers *Dolby* | Remove-AppxPackage
    Get-AppxPackage -allusers *DrawboardPDF* | Remove-AppxPackage
    Get-AppxPackage -allusers *EclipseManager* | Remove-AppxPackage
    Get-AppxPackage -allusers *Facebook* | Remove-AppxPackage
    Get-AppxPackage -allusers *HP* | Remove-AppxPackage
    Get-AppxPackage -allusers *Lenovo* | Remove-AppxPackage
    Get-AppxPackage -allusers *LinkedIn* | Remove-AppxPackage
    Get-AppxPackage -allusers *Netflix* | Remove-AppxPackage
    Get-AppxPackage -allusers *Plex* | Remove-AppxPackage
    Get-appxpackage -allusers *RealtekSemiconductorCorp.RealtekAudioControl* | remove-appxpackage
    Get-appxpackage -allusers *skypeapp* | remove-appxpackage
    Get-appxpackage -allusers *soundrecorder* | remove-appxpackage
    Get-AppxPackage -allusers *SpotifyMusic* | Remove-AppxPackage
    Get-appxpackage -allusers *sway* | remove-appxpackage
    Get-AppxPackage -allusers *Twitter* | Remove-AppxPackage
    Get-AppxPackage -allusers *Viber* | Remove-AppxPackage
    Get-appxpackage -allusers *wallet* | remove-appxpackage
    Get-appxpackage -allusers *windowsphone* | remove-appxpackage
    Get-AppxPackage -allusers *WinZipUniversal* | Remove-AppxPackage
    Get-appxpackage -allusers *zune* | remove-appxpackage

    Start-Sleep -Seconds 5

    #Windows
    get-appxpackage -allusers *phone* | remove-appxpackage
    get-appxpackage -allusers *onenote* | remove-appxpackage
    get-appxpackage -allusers *oneconnect* | remove-appxpackage
    get-appxpackage -allusers *officehub* | remove-appxpackage
    get-appxpackage -allusers *Microsoft.YourPhone* | remove-appxpackage
    get-appxpackage -allusers *microsoft.windowscommunicationsapps* | remove-appxpackage
    get-appxpackage -allusers *Microsoft.WebpImageExtension* | remove-appxpackage
    get-appxpackage -allusers *Microsoft.WebMediaExtensions* | remove-appxpackage
    get-appxpackage -allusers *Microsoft.VP9VideoExtensions* | remove-appxpackage
    get-appxpackage -allusers *Microsoft.ScreenSketch* | remove-appxpackage
    get-appxpackage -allusers *Microsoft.Print3D* | remove-appxpackage
    get-appxpackage -allusers *Microsoft.People* | remove-appxpackage
    get-appxpackage -allusers *Microsoft.MSPaint* | remove-appxpackage
    get-appxpackage -allusers *Microsoft.MixedReality.Portal* | remove-appxpackage
    get-appxpackage -allusers *Microsoft.MicrosoftOfficeHub* | remove-appxpackage
    get-appxpackage -allusers *Microsoft.Microsoft3DViewer* | remove-appxpackage
    get-appxpackage -allusers *Microsoft.HEIFImageExtension* | remove-appxpackage
    get-appxpackage -allusers *Microsoft.Getstarted* | remove-appxpackage
    get-appxpackage -allusers *Microsoft.GetHelp* | remove-appxpackage
    get-appxpackage -allusers *messaging* | remove-appxpackage
    get-appxpackage -allusers *maps* | remove-appxpackage
    get-appxpackage -allusers *feedback* | remove-appxpackage
    get-appxpackage -allusers *commsphone* | remove-appxpackage
    get-appxpackage -allusers *camera* | remove-appxpackage
    get-appxpackage -allusers *bingweather* | remove-appxpackage
    get-appxpackage -allusers *bingsports* | remove-appxpackage
    get-appxpackage -allusers *bingnews* | remove-appxpackage
    get-appxpackage -allusers *bingfinance* | remove-appxpackage
    get-appxpackage -allusers *bing* | remove-appxpackage
    get-appxpackage -allusers *alarms* | remove-appxpackage
    get-appxpackage -allusers *appconnector* | remove-appxpackage
    get-appxpackage -allusers *connectivitystore* | remove-appxpackage
    get-appxpackage -allusers *communicationsapps* | remove-appxpackage
    get-appxpackage -allusers *getstarted* | remove-appxpackage
    get-appxpackage -allusers *Microsoft.Office.Desktop* | remove-appxpackage
    get-appxpackage -allusers *Microsoft.NetworkSpeedTest* | remove-appxpackage
    get-appxpackage -allusers *Microsoft.OfficeLens* | remove-appxpackage
    get-appxpackage -allusers *Microsoft.Whiteboard* | remove-appxpackage
    get-appxpackage -allusers *Microsoft.RemoteDesktop * | remove-appxpackage
    get-appxpackage -allusers *Microsoft.Microsoft3DViewer* | remove-appxpackage
    get-appxpackage -allusers *Microsoft.MicrosoftSolitaireCollection* | remove-appxpackage
    get-appxpackage -allusers *Microsoft.MixedReality.Portal* | remove-appxpackage
    get-appxpackage -allusers *Microsoft.People* | remove-appxpackage
    get-appxpackage -allusers *Microsoft.Office.OneNote* | remove-appxpackage
    get-appxpackage -allusers *Microsoft.SkypeApp* | remove-appxpackage
    get-appxpackage -allusers *Microsoft.Wallet* | remove-appxpackage
    get-appxpackage -allusers *Microsoft.Todos* | remove-appxpackage
    get-appxpackage -allusers *Microsoft.Todos* | remove-appxpackage
    Start-Sleep -Seconds 5

    #The below shouldn't be removed
    #get-appxpackage -allusers *photos* | remove-appxpackage
    #get-appxpackage *sticky* | remove-appxpackage
    #get-appxpackage -allusers *Microsoft.DesktopAppInstaller*
    #get-appxpackage -allusers *appinstaller*
    #get-appxpackage -allusers *Microsoft.Services.Store.Engagement* | remove-appxpackage
    #get-appxpackage -allusers *Microsoft.Advertising.Xaml* | remove-appxpackage
    #get-appxpackage -allusers *Microsoft.StorePurchaseApp* | remove-appxpackage

    #$$$$$$$$$$$$$$$$$$$$$$$$$ The below can't be removed. $$$$$$$$$$$$$$$$$$$$$$$$$#
    #get-appxpackage *Windows.PrintDialog* | remove-appxpackage
    #get-appxpackage *windows.immersivecontrolpanel* | remove-appxpackage
    #get-appxpackage *Windows.CBSPreview* | remove-appxpackage
    #get-appxpackage *people* | remove-appxpackage
    #get-appxpackage *mspaint* | remove-appxpackage
    #get-appxpackage *Microsoft.XboxGameCallableUI* | remove-appxpackage
    #get-appxpackage *Microsoft.Windows.ShellExperienceHost* | remove-appxpackage
    #get-appxpackage *Microsoft.Windows.SecureAssessmentBrowser* | remove-appxpackage
    #get-appxpackage *Microsoft.Windows.SecHealthUI* | remove-appxpackage
    #get-appxpackage *Microsoft.Windows.PinningConfirmationDialog* | remove-appxpackage
    #get-appxpackage *Microsoft.Windows.PeopleExperienceHost* | remove-appxpackage
    #get-appxpackage *Microsoft.Windows.OOBENetworkConnectionFlow* | remove-appxpackage
    #get-appxpackage *Microsoft.Windows.OOBENetworkCaptivePortal* | remove-appxpackage
    #get-appxpackage *Microsoft.Windows.Cortana* | remove-appxpackage
    #get-appxpackage *Microsoft.Windows.ContentDeliveryManager* | remove-appxpackage
    #get-appxpackage *Microsoft.Windows.CloudExperienceHost* | remove-appxpackage
    #get-appxpackage *Microsoft.Windows.CapturePicker* | remove-appxpackage
    #get-appxpackage *Microsoft.Windows.Apprep.ChxApp* | remove-appxpackage
    #get-appxpackage *Microsoft.Win32WebViewHost* | remove-appxpackage
    #get-appxpackage *Microsoft.PPIProjection* | remove-appxpackage
    #get-appxpackage *Microsoft.MicrosoftStickyNotes* | remove-appxpackage
    #get-appxpackage *Microsoft.MicrosoftEdgeDevToolsClient* | remove-appxpackage
    #get-appxpackage *Microsoft.MicrosoftEdge* | remove-appxpackage
    #get-appxpackage *Microsoft.LockApp* | remove-appxpackage
    #get-appxpackage *Microsoft.ECApp* | remove-appxpackage
    #get-appxpackage *Microsoft.BioEnrollment* | remove-appxpackage
    #get-appxpackage *Microsoft.AsyncTextService* | remove-appxpackage
    #get-appxpackage *Microsoft.AAD.BrokerPlugin* | remove-appxpackage
    #get-appxpackage *InputApp* | remove-appxpackage
    #get-appxpackage *holographic* | remove-appxpackage
    #get-appxpackage *F46D4000-FD22-4DB4-AC8E-4E1DDDE828FE* | remove-appxpackage
    #get-appxpackage *E2A4F912-2574-4A75-9BB0-0D023378592B* | remove-appxpackage
    #get-appxpackage *calculator* | remove-appxpackage
    #get-appxpackage *c5e2524a-ea46-4f67-841f-6a9465d9d515* | remove-appxpackage
    #get-appxpackage *1527c705-839a-4832-9118-54d4Bd6a0c89* | remove-appxpackage
    #get-appxpackage *Microsoft.VCLibs.140.00* | remove-appxpackage
    #get-appxpackage *Microsoft.VCLibs.140.00.UWPDesktop* | remove-appxpackage
    #get-appxpackage *Microsoft.UI.Xaml.2.0* | remove-appxpackage
    #get-appxpackage *Microsoft.Windows.XGpuEjectDialog* | remove-appxpackage
    #get-appxpackage *Microsoft.Windows.NarratorQuickStart* | remove-appxpackage
    
}

# Step 3: Disable Suggested Apps.
function DisableSuggestions1 {
    write-host "Step 3: Disable Suggested Apps." -ForegroundColor Yellow
    Set-ItemProperty -Path "HKCU:\Software\Microsoft\Windows\CurrentVersion\ContentDeliveryManager" -Name "SilentInstalledAppsEnabled" -Value 0
    Set-ItemProperty -Path "HKCU:\Software\Microsoft\Windows\CurrentVersion\ContentDeliveryManager" -Name "ContentDeliveryAllowed" -Value 0
}

# Step 4: Remove People Button For Currnt User.
Function HideTaskbarPeopleIcon {
	Write-Output "Step 4: Hiding People icon for current user."
	If (!(Test-Path "HKCU:\SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer\Advanced\People")) {
		New-Item -Path "HKCU:\SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer\Advanced\People" | Out-Null
	}
	Set-ItemProperty -Path "HKCU:\SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer\Advanced\People" -Name "PeopleBand" -Type DWord -Value 0
}

# Step 5: Hide Task View Button
Function HideTaskView {
	Write-Output "Step 5: Hiding Task View button."
	Set-ItemProperty -Path "HKCU:\SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer\Advanced" -Name "ShowTaskViewButton" -Type DWord -Value 0
}

# Step 6: Hide Task View Button For All Users
Function HideTaskViewForAllUser {
	Write-Output "Step 6: Hiding People icon for all users."
	If (!(Test-Path "HKLM:\SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer\Advanced")) {
		New-Item -Path "HKLM:\SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer\Advanced" | Out-Null
	}
	Set-ItemProperty -Path "HKLM:\SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer\Advanced" -Name "ShowTaskViewButton" -Type DWord -Value 0
}

# Step 7: Hide Taskbar Search Icon/Box
Function HideTaskbarSearch {
	Write-Output "Step 7: Hiding Taskbar Search icon / box."
	Set-ItemProperty -Path "HKCU:\SOFTWARE\Microsoft\Windows\CurrentVersion\Search" -Name "SearchboxTaskbarMode" -Type DWord -Value 0
}

# Step 8: Hide Taskbar Search Icon/Box For All Users
Function HideTaskbarSearchForAllUser {
	Write-Output "Step 8: Hiding People icon for all users."
	If (!(Test-Path "HKLM:\Software\Microsoft\Windows\CurrentVersion\Search")) {
		New-Item -Path "HKLM:\Software\Microsoft\Windows\CurrentVersion\Search" | Out-Null
	}
	Set-ItemProperty -Path "HKLM:\Software\Microsoft\Windows\CurrentVersion\Search" -Name "SearchboxTaskbarMode" -Type DWord -Value 0
}

# Step 9: Disable Location Tracking
Function DisableLocationTracking {
	Write-Output "Step 9: Disabling Location Tracking."
	If (!(Test-Path "HKLM:\SOFTWARE\Microsoft\Windows\CurrentVersion\CapabilityAccessManager\ConsentStore\location")) {
		New-Item -Path "HKLM:\SOFTWARE\Microsoft\Windows\CurrentVersion\CapabilityAccessManager\ConsentStore\location" -Force | Out-Null
	}
	Set-ItemProperty -Path "HKLM:\SOFTWARE\Microsoft\Windows\CurrentVersion\CapabilityAccessManager\ConsentStore\location" -Name "Value" -Type String -Value "Deny"
	Set-ItemProperty -Path "HKLM:\SOFTWARE\Microsoft\Windows NT\CurrentVersion\Sensor\Overrides\{BFA794E4-F964-4FDB-90F6-51056BFE4B44}" -Name "SensorPermissionState" -Type DWord -Value 0
	Set-ItemProperty -Path "HKLM:\SYSTEM\CurrentControlSet\Services\lfsvc\Service\Configuration" -Name "Status" -Type DWord -Value 0
}

# Step 10: Disable Automatic Maps Updates
Function DisableMapUpdates {
	Write-Output "Step 10: Disabling automatic Maps updates."
	Set-ItemProperty -Path "HKLM:\SYSTEM\Maps" -Name "AutoUpdateEnabled" -Type DWord -Value 0
}

# Step 11: Disable Feedback
Function DisableFeedback {
	Write-Output "Step 11: Disabling Feedback."
	If (!(Test-Path "HKCU:\SOFTWARE\Microsoft\Siuf\Rules")) {
		New-Item -Path "HKCU:\SOFTWARE\Microsoft\Siuf\Rules" -Force | Out-Null
	}
	Set-ItemProperty -Path "HKCU:\SOFTWARE\Microsoft\Siuf\Rules" -Name "NumberOfSIUFInPeriod" -Type DWord -Value 0
	Set-ItemProperty -Path "HKLM:\SOFTWARE\Policies\Microsoft\Windows\DataCollection" -Name "DoNotShowFeedbackNotifications" -Type DWord -Value 1
	Disable-ScheduledTask -TaskName "Microsoft\Windows\Feedback\Siuf\DmClient" -ErrorAction SilentlyContinue | Out-Null
	Disable-ScheduledTask -TaskName "Microsoft\Windows\Feedback\Siuf\DmClientOnScenarioDownload" -ErrorAction SilentlyContinue | Out-Null
}

# Step 12: Disable Tailored Experiences
Function DisableTailoredExperiences {
	Write-Output "Step 12: Disabling Tailored Experiences."
	If (!(Test-Path "HKCU:\SOFTWARE\Policies\Microsoft\Windows\CloudContent")) {
		New-Item -Path "HKCU:\SOFTWARE\Policies\Microsoft\Windows\CloudContent" -Force | Out-Null
	}
	Set-ItemProperty -Path "HKCU:\SOFTWARE\Policies\Microsoft\Windows\CloudContent" -Name "DisableTailoredExperiencesWithDiagnosticData" -Type DWord -Value 1
}

# Step 13: Disable Advertising ID
Function DisableAdvertisingID {
	Write-Output "Step 13: Disabling Advertising ID."
	If (!(Test-Path "HKLM:\SOFTWARE\Policies\Microsoft\Windows\AdvertisingInfo")) {
		New-Item -Path "HKLM:\SOFTWARE\Policies\Microsoft\Windows\AdvertisingInfo" | Out-Null
	}
	Set-ItemProperty -Path "HKLM:\SOFTWARE\Policies\Microsoft\Windows\AdvertisingInfo" -Name "DisabledByGroupPolicy" -Type DWord -Value 1
}

# Step 14: Disable Error Reporting
Function DisableErrorReporting {
	Write-Output "Step 14: Disabling Error reporting."
	Set-ItemProperty -Path "HKLM:\SOFTWARE\Microsoft\Windows\Windows Error Reporting" -Name "Disabled" -Type DWord -Value 1
	Disable-ScheduledTask -TaskName "Microsoft\Windows\Windows Error Reporting\QueueReporting" | Out-Null
}

# Step 15: Enable NumLock After Startup
Function EnableNumlock {
	Write-Output "Step 15: Enabling NumLock after startup."
	If (!(Test-Path "HKU:")) {
		New-PSDrive -Name HKU -PSProvider Registry -Root HKEY_USERS | Out-Null
	}
	Set-ItemProperty -Path "HKU:\.DEFAULT\Control Panel\Keyboard" -Name "InitialKeyboardIndicators" -Type DWord -Value 2147483650
	Add-Type -AssemblyName System.Windows.Forms
	If (!([System.Windows.Forms.Control]::IsKeyLocked('NumLock'))) {
		$wsh = New-Object -ComObject WScript.Shell
		$wsh.SendKeys('{NUMLOCK}')
	}
}

# Step 16: Show Known File Extensions
Function ShowKnownExtensions {
	Write-Output "Step 16: Showing known file extensions."
	Set-ItemProperty -Path "HKCU:\SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer\Advanced" -Name "HideFileExt" -Type DWord -Value 0
}

# Step 17: Change Default Explorer View To This PC
Function SetExplorerThisPC {
	Write-Output "Step 17: Changing default Explorer view to This PC."
	Set-ItemProperty -Path "HKCU:\SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer\Advanced" -Name "LaunchTo" -Type DWord -Value 1
}

# Step 18: Set Timezone, Region and Language.
function SetTimezone {
    write-host ""
    write-host "Step 18: Set Timezone, Region and Language" -ForegroundColor Yellow
    
    Write-Host "1, QLD"
    Write-Host "2, NT"
    Write-Host "3, WA"
    Write-Host "4, PNG"
    Write-Host "5, TAS, NSW, VIC, ACT"
    Write-Host "6, SA"
    Write-Host "7, NZ"

    if ($Option -eq "Q" -or "3"){
        $Timezone = "1"
    }else{
        $Timezone = Read-Host "Please choose a Timezone"
    }

    if ($Timezone -eq "1"){
        Set-TimeZone -Name "E. Australia Standard Time"
        Set-WinSystemLocale -SystemLocale en-AU
        Set-WinHomeLocation -GeoId 0x9bce09
        Write-Host "Timezone set to AEST" -ForegroundColor Green
    }elseif ($Timezone -eq "2"){
        Set-TimeZone -Name "AUS Central Standard Time"
        Set-WinSystemLocale -SystemLocale en-AU
        Set-WinHomeLocation -GeoId 0x9bce09
        Write-Host "Timezone set to ACST" -ForegroundColor Green
    }elseif ($Timezone -eq "3"){
        Set-TimeZone -Name "W. Australia Standard Time"
        Set-WinSystemLocale -SystemLocale en-AU
        Set-WinHomeLocation -GeoId 0x9bce09
        Write-Host "Timezone set to AWST" -ForegroundColor Green
    }elseif ($Timezone -eq "4"){
        Set-TimeZone -Name "West Pacific Standard Time"
        Set-WinSystemLocale -SystemLocale en-PG # Needs to be changed to the corret region.
        Set-WinHomeLocation -GeoId 0xc2
        Write-Host "Timezone set to PGT" -ForegroundColor Green
    }elseif ($Timezone -eq "5"){
        Set-TimeZone -Name "AUS Eastern Standard Time"
        Set-WinSystemLocale -SystemLocale en-AU
        Set-WinHomeLocation -GeoId 0x9bce09
        Write-Host "Timezone set to AEST/AEDT" -ForegroundColor Green
    }elseif ($Timezone -eq "6"){
        Set-TimeZone -Name "Cen. Australia Standard Time"
        Set-WinSystemLocale -SystemLocale en-AU
        Set-WinHomeLocation -GeoId 0x9bce09
        Write-Host "Timezone set to ACST/ACDT" -ForegroundColor Green
    }elseif ($Timezone -eq "7"){
        Set-TimeZone -Name "New Zealand Standard Time"
        Set-WinSystemLocale -SystemLocale en-NZ
        Set-WinHomeLocation -GeoId 0x9bce09
        Write-Host "Timezone set to NZST" -ForegroundColor Green
    }else{
        write-host "**********************************************" -ForegroundColor Red
        write-host "Please select one of the above options" -ForegroundColor Gray
        write-host "**********************************************" -ForegroundColor Red
        write-host ""
        Start-Sleep -Seconds 2
        SetTimezone
    }
}

# Step 19: Change Default Start Menu Layout
Function ChangeDefaultStartMenuLayout {
    Write-Host ""
    Write-Output "Step 19: Changing default start layout."
    New-Item "C:\Default Start Menu Layout.xml" -ItemType File
    Set-Content "C:\Default Start Menu Layout.xml" '<LayoutModificationTemplate xmlns:defaultlayout="
http://schemas.microsoft.com/Start/2014/FullDefaultLayout" xmlns:start="
http://schemas.microsoft.com/Start/2014/StartLayout" Version="1" xmlns="
http://schemas.microsoft.com/Start/2014/LayoutModification">
    <LayoutOptions StartTileGroupCellWidth="6" />
    <DefaultLayoutOverride>
        <StartLayoutCollection>
            <defaultlayout:StartLayout GroupCellWidth="6" />
        </StartLayoutCollection>
    </DefaultLayoutOverride>
</LayoutModificationTemplate>'
    
    Import-StartLayout -LayoutPath "C:\Default Start Menu Layout.xml" -MountPath "C:\"
    Remove-Item -Path "C:\Default Start Menu Layout.xml" -Force
}

# Step 20: Install Chrome
function InstallChrome {
    Write-Host ""
    write-host "Step 20: Installing Chrome" -ForegroundColor Yellow
    
    if ($Option -eq "Q" -or "3"){
        $ConfirmChrome = "y"
    }else{
        $ConfirmChrome = Read-Host "Do you want to download and install Chrome? (y/n)"
    }

    if ($ConfirmChrome -eq "y"){
        $Path = $env:TEMP
        $Installer = "chrome_installer.exe" 
        Invoke-WebRequest "http://dl.google.com/chrome/install/375.126/chrome_installer.exe" -OutFile $Path\$Installer
        Start-Process -FilePath $Path\$Installer -Args "/silent /install" -Verb RunAs -Wait
        Remove-Item $Path\$Installer

    }elseif ($ConfirmChrome -eq "n") {
        write-host "Skipped installing Chrome"

    }else{    
        YesOrNo
        InstallChrome
    }
}

# Step 21: Install Adobe Reader DC
function InstallAdobeReaderDC {
    Write-Host ""
    write-host "Step 21: Installing Adobe Reader DC" -ForegroundColor Yellow

    if ($Option -eq "Q" -or "3"){
        $ConfirmAdobeReader = "y"
    }else{
        $ConfirmAdobeReader = Read-Host "Do you want to download and install Adobe Reader DC? (y/n)"
    }
    
    if ($ConfirmAdobeReader -eq "y"){
        write-host "Installing Adobe Reader DC, this will take awhile." -ForegroundColor Yellow
        Write-Host "Last I checked the installer was 95,000,000 bytes in size." -ForegroundColor DarkGreen
        $Path = $env:TEMP
        $Installer = "adobeDC.exe" 
        Invoke-WebRequest "http://ardownload.adobe.com/pub/adobe/reader/win/AcrobatDC/1502320053/AcroRdrDC1502320053_en_US.exe" -OutFile $Path\$Installer
        Start-Process -FilePath $Path\$Installer -Args "/sPB /rs" -Verb RunAs -Wait
        Remove-Item $Path\$Installer

    }elseif ($ConfirmAdobeReader -eq "n") {
        write-host "Skipped installing Adobe Acrobat DC"

    }else{    
        YesOrNo
        InstallAdobeReaderDC
    }
}

# Step 22: Renaming The Machine
function RenameMachine {
    Write-Host ""
    write-host "Step 22: Renaming the PC" -ForegroundColor Yellow
    $ConfirmRename = Read-Host "Do you want to rename the computer?"
    
    if ($ConfirmRename -eq "y"){
        $NewName = Read-host "Please enter a new computer name."
        Rename-Computer -NewName $NewName
        
    }elseif ($ConfirmRename -eq "n") {
        write-host "Skipped renaming the computer"

    }else{    
        YesOrNo
        RenameMachine
    }
}

# Step 23: Remove Pre Installed Software
function RemoveInstalledApps {
    Write-Host ""
    write-host "Step 22: Removing Pre Installed Software" -ForegroundColor Yellow
    #Microsoft
    wmic product where "name='Office 16 Click-to-Run Extensibility Component'" uninstall
    wmic product where "name='Office 16 Click-to-Run Localization Component'" uninstall
    wmic product where "name='Office 16 Click-to-Run Licensing Component'" uninstall
    wmic product where "name='Microsoft Office 365 en-us'" uninstall
    wmic product where "name='Microsoft OneNote - en-us'" uninstall
    wmic product where "name='Microsoft Update Health Tools'" uninstall
    wmic product where "name='Windows PC Health Check'" uninstall
    #HP
    wmic product where "name='HP Notifications'" uninstall
    wmic product where "name='HP Performance Advisor'" uninstall
    wmic product where "name='HP Hotkey Support'" uninstall
    wmic product where "name='HP System Default Settings'" uninstall
    wmic product where "name='HP Connection Optimizer'" uninstall
    wmic product where "name='HP Documentation'" uninstall
    wmic product where "name='HP Sure Click'" uninstall
    wmic product where "name='HP Sure Recover'" uninstall
    wmic product where "name='HP Sure Run'" uninstall
    wmic product where "name='HP Sure Sense Installer'" uninstall
    wmic product where "name='HP Client Security Manager'" uninstall 
    wmic product where "name='HP Security Update Service'" uninstall
    wmic product where "name='HP Security Update Service'" uninstall
    wmic product where "name='HP Wolf Security'" uninstall
    Start-Process "C:\Program Files\HP\Documentation\Doc_uninstall.cmd"
    #wmic product where "name=''" uninstall
    #wmic product where "name=''" uninstall
    #wmic product where "name=''" uninstall

    #Get applications names: wmic product get name
}

# Step 24: Install 7Zip
function Install7Zip {
    Write-Host ""
    write-host "Step 24: Installing 7Zip" -ForegroundColor Yellow
    
    if ($Option -eq "Q" -or "3"){
        $Confirm7Zip = "y"
    }else{
        $Confirm7Zip = Read-Host "Do you want to download and install 7Zip? (y/n)"
    }

    if ($Confirm7Zip -eq "y"){
        $Path = $env:TEMP
        $Installer = "7z2106-x64.exe" 
        Invoke-WebRequest "https://www.7-zip.org/a/7z2106-x64.exe" -OutFile $Path\$Installer
        Start-Process -FilePath $Path\$Installer -Args "/S" -Verb RunAs -Wait
        Remove-Item $Path\$Installer

    }elseif ($Confirm7Zip -eq "n") {
        write-host "Skipped installing 7Zip"

    }else{    
        YesOrNo
        Install7Zip
    }
}

# Step 25: Install VLC
function InstallVLC {
    Write-Host ""
    write-host "Step 25: Installing VLC" -ForegroundColor Yellow
    
    if ($Option -eq "Q"){
        $ConfirmVLC = "n"

    }elseif ($Option -eq "3") {
        $ConfirmVLC = "y"

    }else{
        $ConfirmVLC = Read-Host "Do you want to download and install VLC? (y/n)"
    }

    if ($ConfirmVLC -eq "y"){
        #Credit Due: https://pastebin.com/qvDzjzFb - https://www.reddit.com/r/PowerShell/comments/5fxsqj/dynamically_get_current_version_of_vlc_and/

        $vlcURL = "https://download.videolan.org/vlc/last/win32/"
        $getHTML = (New-Object System.Net.WebClient).DownloadString($vlcURL)
        $name = if ($getHTML -match '.+>(vlc-.+\.exe)<.+')
                {
                $Matches[1] 
                } 
        
        $vlcURL = "https://download.videolan.org/vlc/last/win32/$name"
        
        $outPath = "$env:HOMEDRIVE\$env:HOMEPATH\temp"
        $outFile = "$env:HOMEDRIVE\$env:HOMEPATH\temp\installer.exe"
        
        mkdir $outPath -Force
        
        (New-Object System.Net.WebClient).DownloadFile($vlcURL, $outFile)
 
        & $outFile /L=1033 /S

    }elseif ($ConfirmVLC -eq "n") {
        write-host "Skipped installing VLC"

    }else{    
        YesOrNo
        InstallVLC
    }
}

# Step 26: Install OpenVPN
function InstallOpenVPN {
    Write-Host ""
    write-host "Step 26: Installing OpenVPN" -ForegroundColor Yellow
    
    if ($Option -eq "Q"){
        $ConfirmOVPN = "n"

    }elseif ($Option -eq "3") {
        $ConfirmOVPN = "y"
        
    }else{
        $ConfirmOVPN = Read-Host "Do you want to download and install OpenVPN? (y/n)"
    }

    if ($ConfirmOVPN -eq "y"){
        $Path = $env:TEMP
        $Installer = "OpenVPN-2.5.5-I602-amd64.msi" 
        Invoke-WebRequest "https://swupdate.openvpn.org/community/releases/OpenVPN-2.5.5-I602-amd64.msi" -OutFile $Path\$Installer
        cd $env:TEMP
        MsiExec.exe /i OpenVPN-2.5.5-I602-amd64.msi /passive

    }elseif ($ConfirmOVPN -eq "n") {
        write-host "Skipped installing Open VPN"

    }else{    
        YesOrNo
        InstallOpenVPN
    }
}

# Step 27: Install MS Teams
function InstallTeams {
    Write-Host ""
    write-host "Step 27: Installing MS Teams" -ForegroundColor Yellow
    
    if ($Option -eq "Q"){
        $ConfirmTeams = "n"

    }elseif ($Option -eq "3") {
        $ConfirmTeams = "y"
        
    }else{
        $ConfirmTeams = Read-Host "Do you want to download and install MS Teams? (y/n)"
    }

    if ($ConfirmTeams -eq "y"){
        $Path = $env:TEMP
        $Installer = "Teams_windows_x64.msi" 
        Invoke-WebRequest "https://teams.microsoft.com/downloads/desktopurl?env=production&plat=windows&arch=x64&managedInstaller=true&download=true" -OutFile $Path\$Installer
        cd $env:TEMP
        msiexec /i Teams_windows_x64.msi ALLUSERS=1

    }elseif ($ConfirmTeams -eq "n") {
        write-host "Skipped installing MS Teams"

    }else{    
        YesOrNo
        InstallTeams
    }
}

# Selection.
function SelectOption {
    write-host ""
    Write-Host "1. Just remove the Windows Store Apps."
    Write-Host "2. Run the whole script."
    Write-Host "3. CGD QLD Presets"
    Write-Host "Q. Queensland presets."
   
    $Option = Read-Host "Please select an option."

    if ($Option -eq "1"){
        RemoveCrap
        RemoveInstalledApps
    }elseif ($Option -eq "2" -or "Q" -or "3"){
        RemoveCrap
        DisableSuggestions1
        HideTaskbarPeopleIcon
        HideTaskViewForAllUser
        HideTaskbarSearch
        HideTaskbarSearchForAllUser
        DisableLocationTracking
        DisableMapUpdates
        DisableFeedback
        DisableTailoredExperiences
        DisableAdvertisingID
        DisableErrorReporting
        EnableNumlock
        ShowKnownExtensions
        SetExplorerThisPC
        SetTimezone
        ChangeDefaultStartMenuLayout
        InstallChrome
        InstallAdobeReaderDC  
        RenameMachine
        RemoveInstalledApps  
        Install7Zip
        InstallVLC
        InstallOpenVPN
        InstallTeams

    }else{
        write-host "**********************************************" -ForegroundColor Red
        write-host "Please select one of the above options" -ForegroundColor Gray
        write-host "**********************************************" -ForegroundColor Red
        write-host ""
        Start-Sleep -Seconds 2
        SelectOption
    }
}
SelectOption

#=================== End of script, restarting computer. ===================#
Write-Host "The script has finished and the computer will now reboot."
Read-Host -Prompt "Press Enter to continue"
Restart-Computer