$ErrorActionPreference = "Continue"
<# 
.NOTES 
=========================================================================== 
Step 01.
=========================================================================== 
Version 0.1 Mitchell McRae
Date:
=========================================================================== 
Powershell modules required.
None
===========================================================================
.DESCRIPTION 
This Is for configuring the Getac magman tablets for Magman.

Step 01. 
#>

# Runs the script as admin
# This can not be run in a function because it has to be running for the whole script.
If (-NOT ([Security.Principal.WindowsPrincipal] [Security.Principal.WindowsIdentity]::GetCurrent()).IsInRole([Security.Principal.WindowsBuiltInRole]::Administrator))
{
  # Relaunch as an elevated process:
  Start-Process powershell.exe "-File",('"{0}"' -f $MyInvocation.MyCommand.Path) -Verb RunAs
  exit
}

function YesOrNo {
    write-host "**********************************************" -ForegroundColor Red
    write-host "Please enter y/n" -ForegroundColor Gray
    write-host "**********************************************" -ForegroundColor Red
    write-host ""
}